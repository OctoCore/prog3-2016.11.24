package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import common.ResultObject;
import dbconnection.DatabaseConnection;
import template.TemplateManager;

public class LoginServlet extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = 3205355555474414718L;

    private static final Logger logger = LogManager.getLogger(LoginServlet.class);

    private TemplateManager templateManager;
    
    public void init() throws ServletException {
	templateManager = new TemplateManager();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
	logger.debug("LoginServlet.get");
	response.setStatus(200);
	response.setContentType("text/html");
	
	PrintWriter pw = response.getWriter();
	String content = templateManager.getTemplateContent("request.html");
	if (content == null) {
	    logger.warn("Template file beolvasása sikertelen: request.html");
	    pw.println("Hibás template!");
	    return;
	}
	pw.write(content);
	
	//pw.println("Ez itt a válasz");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
	String neptunkod = request.getParameter("neptunkod");


	DatabaseConnection dbConnection = new DatabaseConnection();
	ResultObject ZHResult = dbConnection.getZhResultsByNeptunCode(neptunkod);
	response.setStatus(200);
	response.setContentType("text/html");
	response.setCharacterEncoding("UTF-8");
	PrintWriter pw = response.getWriter();
	if(ZHResult == null){
		pw.println("nem található eredmény! "+neptunkod);
	}
	else
	{
		String content = templateManager.getTemplateContent("response.html");
		if (content == null) {
		    logger.warn("Template file beolvasása sikertelen: response.html");
		    pw.println("Hibás template!");
		    return;
		}
		content = content
				.replaceAll("#%R_NEV%#", ZHResult.getNev())
				.replaceAll("#%R_NEPTUN%#", ZHResult.getNeptunkod())
				.replaceAll("#%R_ZH_PONT%#", ZHResult.getPontszam().toString())
				.replaceAll("#%R_MEGJ%#", ZHResult.getMegjegyzes());
		
		pw.write(content);
	}
	
    }

    public void destroy() {

    }

}
