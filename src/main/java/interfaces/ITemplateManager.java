package interfaces;

public interface ITemplateManager {
 public String getTemplateContent(String templateName);
}
