package dbconnection;

import java.io.IOException;
import java.sql.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import common.ResultObject;
import config.Config;

public class DatabaseConnection {
    // JDBC Driver
    private static final String JDBC_Driver = "com.mysql.jdbc.Driver";
    private static final String DB_Prefix = "jdbc:mysql://";
    
    private static final Logger logger = LogManager.getLogger(DatabaseConnection.class);

    // Teljes sztring : "jdbc:mysql://localhost/School"

    public ResultObject getZhResultsByNeptunCode(String neptuncode) {
	logger.debug("checkUserPassword hivas folyamatban...");

	Boolean result = false;

	Connection con = null;
	Statement stmt = null;
	ResultObject zhresults = null;

	try {
	    // Beállítjuk a JDBC driver elérési útját
	    String dbHost = Config.getProperty("mysql.host", "localhost");
	    String dbPort = Config.getProperty("mysql.port", "3306");
	    String dbDatabase = Config.getProperty("mysql.database", "webproject");
	    String dbUsername = Config.getProperty("mysql.username", "webproject");
	    String dbPassword = Config.getProperty("mysql.password", "P4ssw0rd");

	    logger.trace("MySQL host connection: host name: " + dbHost + ", Port: " + dbPort);
	    Class.forName(JDBC_Driver);

	    con = DriverManager.getConnection(DB_Prefix + dbHost + ":" + dbPort + "/" + dbDatabase, dbUsername,
		    dbPassword);
	    stmt = con.createStatement();

	    ResultSet rs = stmt.executeQuery("SELECT * FROM results WHERE r_neptun='"+neptuncode+"'");

	    if (rs.wasNull()) {
		logger.error("Result set from MySQL was null. Username: " + neptuncode);
		return null;
	    }
	    
	    if (!rs.isBeforeFirst()) {
		logger.debug("A felhasználó nem található az adatbázisban! Username: " + neptuncode);
		return null;
	    }
	    
	    zhresults = new ResultObject();
	    
	    while (rs.next()) {
	    	logger.debug(rs.getString("r_neptun"));	
	    	
			zhresults.setNeptunkod(rs.getString("r_neptun"));
			zhresults.setNev(rs.getString("r_nev"));
			zhresults.setPontszam(rs.getInt("r_zh_pont"));
			zhresults.setMegjegyzes(rs.getString("r_megj"));
	    }

	    rs.close();

	    stmt.close();
	    con.close();
	    
	} catch (ClassNotFoundException e) {
	    logger.error("Hiba a JDBCDriver inicializálása közben: " + e.getMessage());

	} catch (SQLException e) {
	    logger.error("SQL kivétel! " + e.getMessage());
	} catch (IOException e) {
	    logger.error("IO hiba! " + e.getMessage());
	} finally {
	    if (stmt != null) {
		try {
		    stmt.close();
		} catch (SQLException e) {
		    logger.error("SQL kivétel finally ág stmt.close-nál! " + e.getMessage());
		}
	    }
	    if (con != null) {
		try {
		    con.close();
		} catch (SQLException e) {
		    logger.error("SQL kivétel a connection lezárásánál! " + e.getMessage());
		}
	    }
	}

	logger.warn("Sikertelen adatbázis lekérdezés. Username: " + neptuncode);
	return zhresults;

    }

    public static void main(String[] args) {
	// (new DatabaseConnection()).checkUserPassword("Bela3", "Bubosvocsok");
	// System.getProperties().setProperty("log4j.configurationFile",
	// "log4j.properties");
	Logger l = LogManager.getLogger(DatabaseConnection.class);
	l.debug("Configuration File Defined To Be :: " + System.getProperty("log4j.configurationFile"));
	l.debug("Most jól logolunk!");
    }
}
