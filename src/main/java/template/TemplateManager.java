package template;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import interfaces.ITemplateManager;

public class TemplateManager implements ITemplateManager {
    private static final Logger logger = LogManager.getLogger(TemplateManager.class);

    @Override
    public String getTemplateContent(String templateName) {
	if (templateName == null) {
	    logger.error("TemplateName paraméter értéke null!");
	    return null;
	}
	if (templateName.isEmpty()) {
	    logger.error("TemplateName paraméter értéke üres sztring!");
	    return null;
	}
	try (InputStream in = TemplateManager.class.getClassLoader()
		.getResourceAsStream("templates/" + templateName)) {
	    
	    String content = null;
	    content = IOUtils.toString(in, Charset.forName("UTF-8"));
	    return content;
	    
	} catch (IOException e) {
	    logger.error("IO hiba! Kivétel: " + e.getMessage());
	    return null;
	}
    }

}
