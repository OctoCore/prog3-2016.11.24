package common;

public class ResultObject
{
	private String neptunkod;
	private String nev;
	private Integer pontszam;
	private String megjegyzes;
	
	public String getNeptunkod()
	{
		return neptunkod;
	}
	public void setNeptunkod(String neptunkod)
	{
		this.neptunkod = neptunkod;
	}
	public String getNev()
	{
		return nev;
	}
	public void setNev(String nev)
	{
		this.nev = nev;
	}
	public Integer getPontszam()
	{
		return pontszam;
	}
	public void setPontszam(Integer pontszam)
	{
		this.pontszam = pontszam;
	}
	public String getMegjegyzes()
	{
		return megjegyzes;
	}
	public void setMegjegyzes(String megjegyzes)
	{
		this.megjegyzes = megjegyzes;
	}
	

}
